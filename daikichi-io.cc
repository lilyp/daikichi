/* Copyright © 2022 Liliana Prikler <liliana.prikler@gmail.com>
 *
 * This game is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.

 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <concepts>
#include <iostream>
#include "daikichi.h"

namespace
{
  std::ostream &
  write_be (std::ostream &out, std::unsigned_integral auto n, size_t nb)
  {
    std::string s;
    try
      {
        s.resize (nb);
      }
    catch (const std::exception &e)
      {
        out.setstate (std::ios::failbit);
        return out;
      }
    for (size_t i = 0; i < nb; ++i)
      s[i] = static_cast<unsigned char> (n >> (8 * i) & 0xff);
    return out.write (s.data (), nb);
  }

  std::ostream &
  write_be (std::ostream &out, std::signed_integral auto n, size_t nb)
  {
    using U = std::make_unsigned_t<decltype (n)>;
    return write_be (out, static_cast<U> (n), nb);
  }

  std::istream &
  read_be (std::istream &in, std::unsigned_integral auto &n, size_t nb)
  {
    using T = std::remove_reference<decltype (n)>::type;
    std::string s;
    try
      {
        s.resize (nb);
      }
    catch (const std::exception &e)
      {
        in.setstate (std::ios::failbit);
        return in;
      }
    std::istream &ret = in.read (s.data (), nb);
    if (ret)
      {
        n = static_cast<T> (0);
        for (size_t i = 0; i < nb; ++i)
          n |= static_cast<T> (s[i] & 0xff) << (8 * i);
      }
    return ret;
  }

  std::istream &
  read_be (std::istream &in, std::signed_integral auto &n, size_t nb,
           bool expand = true)
  {
    using U = std::make_unsigned_t<std::remove_cvref_t<decltype (n)>>;
    auto m = static_cast<U> (n);
    auto &ret = read_be (in, m, nb);
    n = static_cast<std::remove_cvref_t<decltype (n)>> (m);
    if (expand && n && n & (1 << (8 * nb - 1)))
      for (size_t i = nb; i < sizeof (n); ++i)
        n |= 0xff << (8 * i);
    return ret;
  }

}

namespace daikichi
{
  std::istream &
  operator>> (std::istream &i, header &header)
  {
#define check_fail(i) do { if (!i) return i; } while (false)
#define fail(i) do { i.setstate (std::ios::failbit); return i; } while (false)
    size_t n_strings = 0;
    std::streamoff off;
    unsigned long flags;

    i.read (reinterpret_cast<char *> (&header.magic), MAGIC_LEN);
    check_fail (i);
    read_be (i, header.version, sizeof (header.version));
    if (header.version != 2)
      fail (i);
    read_be (i, header.nb_size, sizeof (header.nb_size));
    check_fail (i);
    read_be (i, header.nb_off, sizeof (header.nb_off));
    check_fail (i);
    if (header.nb_size > sizeof (size_t) ||
        header.nb_off > sizeof (std::streamoff))
      fail (i);

    static_assert(sizeof (unsigned long) >= decltype(header.flags)().size () / 8);
    read_be (i, flags, header.flags.size () / 8);
    header.flags = flags;

    read_be (i, n_strings, header.nb_size);
    try
      {
        header.offsets.reserve (n_strings);
      }
    catch (const std::exception& e)
      {
        fail (i);
      }

    while (n_strings--)
      {
        read_be (i, off, header.nb_off, /* expand */ false);
        check_fail (i);
        header.offsets.push_back (off);
      }

    // we only store one offset per string, so we require sortedness
    if (!std::ranges::is_sorted (header.offsets))
      fail (i);

    if (header.offsets.size ())
      {
        auto pos = i.tellg ();
        i.seekg (header.offsets.back ());
        check_fail (i);
        i.seekg (pos);
      }

    return i;
#undef fail
#undef check_fail
  }

  std::ostream &
  operator<< (std::ostream &o, const header& header)
  {
    o.write (header.magic, MAGIC_LEN);
    write_be (o, header.version, sizeof (header.version));
    write_be (o, header.nb_size, sizeof (header.nb_size));
    write_be (o, header.nb_off, sizeof (header.nb_off));

    static_assert(sizeof (unsigned long) >= decltype(header.flags)().size () / 8);
    write_be (o, header.flags.to_ulong (), header.flags.size () / 8);
    write_be (o, header.offsets.size (), header.nb_size);
    for (const auto offset : header.offsets)
      write_be (o, offset, header.nb_off);
    return o;
  }
}
