# 大吉

Daikichi (大吉, lit. "great blessing") is an alternative implementation of the
Unix `fortune` command.

## Why Daikichi?

Unfortunately, the `fortune` implementation used in most Linux distributions
(alas, we shall not link to it, but it ought to be easy to find) includes many
pointlessly offensive quotes.  In order not to alienate users who just wanted a
quick laugh, they are "cleverly" hidden behind a switch.  This support for
offensive quotes is deeply engrained into the codebase and thus difficult to
replace with a more meaningful choice.  Furthermore, the code base itself
contains questionable comments.

More subtly, most `fortune` implementations put the power to decide which
fortunes to include and which to label as offensive in the hands of a system
administrator reigning over some bespoke directory
(typically `/usr/share/games/fortunes`).

Daikichi addresses both of these problems by utilizing the revolutionary
concept of environment variables.  Users can point `DAIKICHI_FORTUNE_PATH`
(alternatively `FORTUNE_PATH`) to their own obscure in-jokes, or to a
directory of fortunes, that they've collectively agreed to being funny.
It further comes *without any fortune files* to instead focus on providing
a useful tool.  Thus, the only jokes forced by Daikichi onto users are our
easter eggs, which are funny by definition.

## Differences and incompatibilities with other `fortune`s

- As mentioned above, there is no special support for offensive fortunes,
  nor will there ever be.
- There is no `strfile` or `unstr`.
  Daikichi uses `daikichi pack` and `daikichi unpack` respectively.
- Daikichi's data files have a different format from that used by BSD
  `fortune`.  In particular, it includes all the strings (so you don't need
  to keep the original files around).
  For compatibility with BSD's fortune, Daikichi disregards any files with an
  invalid header.

## Limitations

- There is currently no support for distinguishing long and short
  fortunes.
- There is currently no support for matching fortunes with regular
  expressions.
- There is no support for your favourite locale.  Use UTF-8.

## Testing Daikichi

Since Daikichi does not include any fortunes, testing it is a little
tricker than testing other `fortune` implementations.  To aid this process
a little, daikichi installs two test scripts to `libexecdir`:

- `test-strings` verifies that your strings file is canonical by doing a
  round-trip through `daikichi pack` and `daikichi unpack`.
- `test-dat` verifies that the datfile can be cleanly unpacked and repacked.

Note, that there is currently no test checking that a given dat file corresponds
to a given strings file, though one can manually invoke `daikichi unpack`
followed by `diff`.