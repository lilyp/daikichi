(use-modules (guix packages)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix build-system meson)
             (gnu packages base)
             (gnu packages bash)
             (gnu packages multiprecision)
             (gnu packages pretty-print)
             (gnu packages pkg-config))

(define current-directory (dirname (current-filename)))

(package
  (name "daikichi")
  (version "0.3.1")
  (source
   (local-file current-directory
               #:recursive? #t
               #:select?
               (if (file-exists? (string-append current-directory "/.git"))
                   (git-predicate current-directory)
                   (const #t))))
  (build-system meson-build-system)
  (arguments
   (list #:phases
         #~(modify-phases %standard-phases
             (add-after 'unpack 'hard-code-test-paths
               (lambda* (#:key inputs #:allow-other-keys)
                 (substitute* (list "test-dat.in" "test-strings.in")
                   (("(basename|cmp|diff|mktemp|rm|sed|seq)" cmd)
                    (search-input-file inputs (string-append "bin/" cmd)))))))))
  (inputs (list bash-minimal coreutils gmp fmt sed))
  (native-inputs (list pkg-config))
  (home-page "https://gitlab.com/lilyp/daikichi")
  (synopsis "Display random fortunes")
  (description "Daikichi provides an alternative implementation of
@command{fortune}.")
  (license license:gpl3+)
  (native-search-paths
   (list (search-path-specification
          (variable "DAIKICHI_FORTUNE_PATH")
          (files '("share/fortunes"))))))
