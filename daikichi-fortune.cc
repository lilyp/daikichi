/* Copyright © 2022 Liliana Prikler <liliana.prikler@gmail.com>
 *
 * This game is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.

 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <random>
#include <vector>

#include <getopt.h>
#include <gmpxx.h>

#include "daikichi.h"

#ifndef PATH_SEPARATOR_CHAR
#  if defined _WIN32 && !defined __CYGWIN__
#    define PATH_SEPARATOR_CHAR		';'
#  else
#    define PATH_SEPARATOR_CHAR		':'
#  endif
#endif /* PATH_SEPARATOR_CHAR */

std::vector<std::filesystem::path>
fortune_path ()
{
  std::vector<std::filesystem::path> paths;

  const char *fortune_path_env = getenv ("DAIKICHI_FORTUNE_PATH");
  if (fortune_path_env == nullptr)
    fortune_path_env = getenv ("FORTUNE_PATH");

  if (fortune_path_env != nullptr)
    {
      std::istringstream iss(fortune_path_env);
      std::string dir;

      while (std::getline (iss, dir, PATH_SEPARATOR_CHAR))
        paths.push_back (dir);
    }

  return paths;
}

template <typename StreamIterator, typename HeaderIterator>
void
find_fortunes (std::filesystem::path directory,
               StreamIterator fit,
               HeaderIterator hit)
{
  if (!std::filesystem::is_directory (directory))
    {
      std::cerr << "Not a directory: " << directory << std::endl;
      return;
    }

  for (const auto &dir_entry : std::filesystem::directory_iterator (directory))
    {
      if (!dir_entry.is_regular_file ())
        continue;

      std::ifstream f(dir_entry.path ());
      daikichi::header h;

      f >> h;
      if (f.good () && h.offsets.size ())
        {
          *fit = std::move(f);
          *hit = h;
          fit++;
          hit++;
        }
    }
}

namespace daikichi
{
  command
  fortune ()
  {
    return
      {
        "",
        "\
Print a random quote.\n\
\n\
Available options:\n\
  -h, --help                     Show this help.\n\
  -v, --version                  Print the version of this program.",
        [](int argc, char **argv) -> std::variant<args, status>
        {
          static const struct option opts[] =
          {
            {"--help", no_argument, nullptr, 'h'},
            {"--version", no_argument, nullptr, 'v'}
          };

          int v;
          while ((v = getopt_long (argc, argv, "hv", opts, nullptr)) != -1)
            {
              switch (v)
                {
                case 'h':
                  return status::PRINT_HELP;
                case 'v':
                  return status::PRINT_VERSION;
                }
            }

          return params::fortune{};
        },
        []([[maybe_unused]] args args) {
          std::vector<std::ifstream> files;
          std::vector<daikichi::header> headers;

          for (auto dir: fortune_path ())
            find_fortunes (dir,
                           std::back_inserter (files),
                           std::back_inserter (headers));

          if (headers.size () == 0)
            {
              std::cerr << "no fortunes found" << std::endl;
              return status::FAILURE;
            }

          /* Here, we make two assumptions.  First, that by opening enough
           * files, we can somehow get more than SIZE_MAX strings.  Second,
           * that if we do, mpz_t can store size_t and vice versa.
           * The second one is only valid if size_t == unsigned long.
           * Sorry, "Win"dows fans. */

          std::vector<mpz_class> cweights;
          cweights.reserve (headers.size ());
          mpz_class weight{0};

          for (const auto &header : headers)
            {
              weight += header.offsets.size ();
              cweights.push_back (weight);
            }

          std::random_device rd;
          gmp_randclass rnd (gmp_randinit_default);
          rnd.seed (rd ());
          mpz_class z = rnd.get_z_range (cweights.back ());

          size_t choice0 = std::distance (cweights.begin (),
                                          std::lower_bound (cweights.begin (),
                                                            cweights.end (),
                                                            z));
          size_t choice1 = mpz_class (cweights.at (choice0) - z).get_ui ();

          std::cout
            << extract (files.at (choice0), headers.at (choice0), choice1)
            << std::flush;

          return status::SUCCESS;
        }
      };
  }
}
