/* Copyright © 2022 Liliana Prikler <liliana.prikler@gmail.com>
 *
 * This game is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.

 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <random>

#include <fmt/format.h>
#include <getopt.h>

#include "daikichi.h"

namespace daikichi
{
  std::string
  extract (std::istream& in, const header &header, size_t n)
  {
    if (n > header.offsets.size ())
      {
        auto err = fmt::format ("{} > {}", n, header.offsets.size ());
        throw std::out_of_range (err);
      }

    if (n > 0)
      in.seekg (header.offsets.at (n - 1));
    else
      in.seekg (strings_offset (header));

    std::streamsize sz = header.offsets.at (n) - in.tellg ();
    std::string s;
    s.resize (sz);
    in.read (s.data (), sz);
    return s;
  }

  command randstr ()
  {
    return
      {
        "DAT",
        "\
Print a random quote from DAT.\n\
\n\
Available options:\n\
  -h, --help                     Show this help.\n\
  -v, --version                  Print the version of this program.",
        {
          [](int argc, char **argv) -> std::variant<args, status>
          {
            static const struct option opts[] =
            {
              {"--help", no_argument, nullptr, 'h'},
              {"--version", no_argument, nullptr, 'v'}
            };

            int v;
            while ((v = getopt_long (argc, argv, "hv", opts, nullptr)) != -1)
              {
                switch (v)
                  {
                  case 'h':
                    return status::PRINT_HELP;
                  case 'v':
                    return status::PRINT_VERSION;
                  }
              }

            if ((argc - optind) != 1)
              return std::monostate{};

            return params::randstr {argv[optind]};
          }
        },
        [](args args)
        {
          auto p = std::get_if<params::randstr> (&args);

          if (p == nullptr)
            return status::INVALID_ARGS;

          daikichi::header header;
          std::ifstream fin (p->from, std::ios_base::in | std::ios_base::binary);

          if (!fin)
            {
              std::cerr << "no such file or directory: " << p->from << std::endl;
              return status::FAILURE;
            }

          fin >> header;

          if (!fin)
            {
              std::cerr << "bad header" << std::endl;
              return status::FAILURE;
            }
          else if (!header.offsets.size ())
            {
              std::cerr << "no fortunes found" << std::endl;
              return status::FAILURE;
            }

          std::random_device rd;
          std::mt19937 gen (rd ());
          std::uniform_int_distribution<> distrib (0,
                                                   header.offsets.size () - 1);

          auto n = distrib (gen);
          std::cout << extract (fin, header, n) << std::flush;

          return status::SUCCESS;
        }
      };

  }
}
