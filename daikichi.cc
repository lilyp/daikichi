/* Copyright © 2022 Liliana Prikler <liliana.prikler@gmail.com>
 *
 * This game is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.

 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <functional>
#include <iostream>
#include <map>
#include <random>
#include <string>
#include <string_view>
#include <utility>

#include "daikichi.h"

static std::string_view bin;

daikichi::command help ();
daikichi::command version ();

const std::map<std::string, daikichi::command>
commands =
  {
    {"help", help ()},
    {"version", version ()},
    {"fortune", daikichi::fortune ()},
    {"pack", daikichi::pack ()},
    {"randstr", daikichi::randstr ()},
    {"unpack", daikichi::unpack ()}
  };

daikichi::command
help ()
{
  return
    {
      "[COMMAND]", "Print the help for COMMAND.",
      [](int argc, char **argv)
      {
        if (argc > 1 && commands.contains (argv[1]))
          {
            auto cmd = commands.at (argv[1]);
            std::cout << bin << " "
                      << argv[1] << " " << cmd.usage << std::endl;
            if (cmd.help.size ())
              std::cout << '\n' << cmd.help << std::endl;
          }
        else
          {
            std::cout << "Available commands:" << std::endl;
            for (auto cmd: commands)
              {
                if (cmd.first.at (0) != '-')
                  std::cout << "  " << cmd.first << std::endl;
              }
          }
        return daikichi::status::SUCCESS;
      },
      []([[maybe_unused]] daikichi::args args )
      {
#if __cpp_lib_unreachable
        std::unreachable ();
#endif
        return daikichi::status::FAILURE;
      }
    };
}

daikichi::command
version ()
{
  return
    {
      "", "Print versioning information.",
      []([[maybe_unused]] int argc, [[maybe_unused]] char **argv)
      {
        return daikichi::status::PRINT_VERSION;
      },
      []([[maybe_unused]] daikichi::args args)
      {
#if __cpp_lib_unreachable
        std::unreachable ();
#endif
        return daikichi::status::PRINT_VERSION;
      }
    };
}

int
main(int argc, char **argv)
{
  if (argc < 1)
    {
      std::cerr << "Do you have nothing better to do?" << std::endl;
      std::random_device rd;
      std::mt19937 gen (rd ());
      std::uniform_int_distribution<> distrib(1, 127);
      return distrib (gen);
    }

  bin = argv[0];

  std::map<std::string, daikichi::command> all_commands{commands};

  all_commands["-h"]        = all_commands["help"];
  all_commands["--help"]    = all_commands["help"];
  all_commands["-v"]        = all_commands["version"];
  all_commands["--version"] = all_commands["version"];

  if (argc < 2)
    {
      std::cerr << "Usage: " << bin << std::endl;
      return EXIT_FAILURE;
    }

  argc -= 1;
  argv++;

  if (!all_commands.contains (argv[0]))
    {
      std::cerr << "no such command: " << argv[0] << std::endl;
      return EXIT_FAILURE;
    }

  auto cmd = all_commands.at (argv[0]);
  switch (cmd (argc, argv))
    {
    case daikichi::status::PRINT_VERSION:
      std::cout << "Daikichi " << daikichi::VERSION << std::endl;
      return EXIT_SUCCESS;
    case daikichi::status::PRINT_HELP:
      std::cout << bin << " " << argv[0] << " " << cmd.usage << std::endl;
      if (cmd.help.size ())
        std::cout << '\n' << cmd.help << std::endl;
      return EXIT_SUCCESS;
    case daikichi::status::SUCCESS:
      return EXIT_SUCCESS;
    case daikichi::status::INVALID_ARGS:
      std::cerr << "Usage: " << bin << " " << argv[0]
                << " " << cmd.usage << std::endl;
      [[fallthrough]];
    case daikichi::status::FAILURE:
    default:
      return EXIT_FAILURE;
    }
}
