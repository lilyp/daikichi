#include "daikichi.h"
#include <random>

int
main (int argc, char **argv)
{
  if (argc < 1)
    {
      std::cerr << "Do you have nothing better to do?" << std::endl;
      std::random_device rd;
      std::mt19937 gen (rd ());
      std::uniform_int_distribution<> distrib(1, 127);
      return distrib (gen);
    }

  auto cmd = daikichi::COMMAND ();
  switch (cmd (argc, argv))
    {
    case daikichi::status::PRINT_VERSION:
      std::cout << "Daikichi " << daikichi::VERSION << std::endl;
      return EXIT_SUCCESS;
    case daikichi::status::PRINT_HELP:
      {
        std::cout << argv[0] << " " << cmd.usage << std::endl;
        if (cmd.help.size ())
          std::cout << '\n' << cmd.help << std::endl;
        return EXIT_SUCCESS;
      }
    case daikichi::status::SUCCESS:
      return EXIT_SUCCESS;
    case daikichi::status::INVALID_ARGS:
      std::cerr << "Usage: " << argv[0]
                << " " << cmd.usage << std::endl;
      [[fallthrough]];
    case daikichi::status::FAILURE:
    default:
      return EXIT_FAILURE;
    }
}
