/* Copyright © 2022 Liliana Prikler <liliana.prikler@gmail.com>
 *
 * This game is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.

 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm> // std::transform
#include <bit>
#include <cstring> // memcpy
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

#include <fmt/format.h>
#include <getopt.h>

#include "daikichi.h"

namespace
{
  static const struct option options[] =
    {
      {"help", no_argument, nullptr, 'h'},
      {"version", no_argument, nullptr, 'v'},
      {"delimiter", required_argument, nullptr, 'd'}
    };

  std::variant<daikichi::args, daikichi::status>
  pack_args (int argc, char **argv)
  {
    std::string delim{"%"};

    int v;
    while ((v = getopt_long (argc, argv, "hvd:", options, nullptr)) != -1)
      {
        switch (v)
          {
          case 'h':
            return daikichi::status::PRINT_HELP;
          case 'v':
            return daikichi::status::PRINT_VERSION;
          case 'd':
            delim = optarg;
            break;
          }
      }

    std::shared_ptr<std::istream> in (nullptr);
    std::shared_ptr<std::ostream> out (nullptr);

    if ((argc - optind) > 2)
      return std::monostate{};

    if ((argc - optind) > 0)
      {
        in.reset (new std::ifstream{argv[optind]});
        if (!(*in))
          {
            std::cerr << "no such file or directory: " << argv[optind]
                      << std::endl;
            return daikichi::status::FAILURE;
          }
      }
    else
      in.reset (&std::cin, []([[maybe_unused]] auto p){});

    if ((argc - optind) > 1)
      out.reset (new std::ofstream{argv[optind + 1]});
    else
      out.reset (&std::cout, []([[maybe_unused]] auto p){});

    std::vector<std::string> delims;
    std::stringstream sb{delim};
    std::string s;
    while (getline (sb, s))
      delims.push_back (s);

    return daikichi::params::pack{in, out, std::move (delims)};
  }

  template<typename InputIt, typename T>
  bool contains(InputIt begin, InputIt end, const T& value)
  {
    return find (begin, end, value) != end;
  }
}

namespace
{
  constexpr uint16_t
  required_bytes (std::unsigned_integral auto n)
  {
    constexpr const auto allowed_bytes =
      std::numeric_limits<uint16_t>::max ();

    int used_bits = std::bit_width (n);
    int unpadded_used_bytes = used_bits / 8 + (used_bits % 8 ? 1 : 0);
    if (unpadded_used_bytes < 0)
      throw std::out_of_range (fmt::format ("0 <= {} <= {}",
                                            unpadded_used_bytes,
                                            allowed_bytes));

    auto used_bytes = std::bit_ceil (static_cast<unsigned> (unpadded_used_bytes));
    if (used_bytes > allowed_bytes)
      throw std::out_of_range (fmt::format ("0 <= {} <= {}", used_bytes,
                                            allowed_bytes));
    return static_cast<uint16_t> (used_bytes);
  }

  constexpr uint16_t
  required_bytes (std::signed_integral auto n)
  {
    using U = std::make_unsigned_t<decltype (n)>;
    return required_bytes (static_cast<U> (n));
  }

  bool
  compress (daikichi::header &header)
  {
    using std::numeric_limits;
    using std::streamoff;

    auto &offsets = header.offsets;

    if (!offsets.size ())
      {
        header.nb_size = 0;
        header.nb_off = 0;
        return true;
      }

    header.nb_size = required_bytes (header.offsets.size ());
    header.nb_off = sizeof (std::streamoff);

    streamoff old_size, new_size;
    do
      {
        old_size = strings_offset (header);
        // XXX: perhaps a little too pessimistic ...
        if (offsets.back () > numeric_limits<streamoff>::max () - old_size)
          return false;
        header.nb_off = required_bytes (offsets.back () + old_size);
        new_size = strings_offset (header);
      } while (old_size > new_size);

    return true;
  }

}

namespace daikichi
{
  std::streamoff
  strings_offset (const header &header)
  {
    std::streamoff offset = 16; /* magic, version, lc_nstrings, lc_size, flags */
    offset += header.nb_size;   /* offsets.size () */
    offset += header.offsets.size () * header.nb_off; /* offsets */
    return offset;
  }

  command
  pack ()
  {
    return
      {
        "[OPTIONS] [QUOTES] [DAT]",
        "\
Store QUOTES in DAT.\n\
\n\
If no QUOTES file is given, read them from standard input.  Likewise, if\n\
DAT is missing, write to standard output.\n\
\n\
Available options:\n\
  -h, --help                     Show this help.\n\
  -v, --version                  Print the version of this program.\n\
  -d DELIM, --delimiter=DELIM    Use DELIM as delimiter rather than '%'.\n\
                                 Multiple delimiters, each separated by\n\
                                 a newline, are supported.",
        pack_args,
        [](args args)
        {
          using std::streamoff;
          using std::numeric_limits;

          auto p = std::get_if<params::pack> (&args);

          if (p == nullptr)
            return status::INVALID_ARGS;

          daikichi::header header;
          std::vector<streamoff> &offsets = header.offsets;

          std::stringstream sb;
          std::string s;

          while (std::getline (*p->from, s))
            {
              if (contains (p->delimiters.begin (), p->delimiters.end (), s))
                offsets.push_back (sb.tellp ());
              else
                sb << s << '\n';
            }

          if (sb.tellp () && offsets.back () != sb.tellp ())
            offsets.push_back (sb.tellp ());

          std::memcpy (header.magic, daikichi::MAGIC, sizeof(header.magic));
          header.version = 2;

          if (!compress (header))
            {
              std::cerr << "input file too large" << std::endl;
              return status::FAILURE;
            }

          auto offset0 = strings_offset (header);
          std::transform (offsets.cbegin (), offsets.cend (),
                          offsets.begin (),
                          [offset0](auto o) { return o + offset0;});

          *p->to << header;
          *p->to << sb.str ();
          p->to->flush ();

          return status::SUCCESS;

        }
      };
  }

  command
  unpack ()
  {
    return
      {
        "[OPTIONS] [DAT] [QUOTES]",
        "\
Unpack QUOTES from DAT.\n\
\n\
If no DAT file is given, read it from standard input.  Likewise, if the\n\
QUOTES file is missing, write quotes to standard output.\n\
\n\
Available options:\n\
  -h, --help                     Show this help.\n\
  -v, --version                  Print the version of this program.\n\
  -d DELIM, --delimiter=DELIM    Use DELIM as delimiter rather than '%'.\n\
                                 Only the first line will be used.",
        pack_args,
        [](args args)
        {
          using std::streamoff;
          using std::numeric_limits;

          auto p = std::get_if<params::pack> (&args);

          if (p == nullptr)
            return status::INVALID_ARGS;

          if (!p->delimiters.size())
            {
              std::cerr << "missing delimiter" << std::endl;
              return status::FAILURE;
            }

          daikichi::header header;

          *p->from >> header;

          if (!p->from)
            {
              std::cerr << "bad header" << std::endl;
              return status::FAILURE;
            }

          bool first = true;
          std::string buf;

          /* We could use extract () here, but this is more efficient. */
          for (auto offset : header.offsets)
            {
              std::streamsize sz = offset - p->from->tellg ();
              buf.resize (sz);
              p->from->read (buf.data (), sz);
              if (!first)
                *p->to << p->delimiters.at (0) << std::endl;

              *p->to << buf;
              first = false;
            }

          p->to->flush ();
          return status::SUCCESS;

        }
      };
  }
}
